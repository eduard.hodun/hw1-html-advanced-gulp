const burgerMenuLines = document.querySelector('.burger__menu-lines');
const burgerBtnLines = document.querySelector('.burger__btn-lines');
const burgerBtnPlus = document.querySelector('.burger__btn-plus');
const burgerMenuList = document.querySelector('.burger__menu-list');

burgerMenuLines.addEventListener('click', function(){
    burgerBtnPlus.classList.toggle('burger__active');
    burgerBtnLines.classList.toggle('burger__active');

    burgerMenuList.classList.toggle('hide')
})


